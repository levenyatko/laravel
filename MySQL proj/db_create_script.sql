-- MySQL Script generated by MySQL Workbench
-- Fri Apr 13 20:17:43 2018
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`peoples`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`peoples` (
  `people_id` INT UNSIGNED NOT NULL,
  `first_name` VARCHAR(255) NOT NULL,
  `last_name` VARCHAR(255) NOT NULL,
  `isMale` TINYINT(1) NOT NULL,
  `avatar_image_id` INT UNSIGNED NULL,
  PRIMARY KEY (`people_id`),
  UNIQUE INDEX `people_id_UNIQUE` (`people_id` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`appointment_types`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`appointment_types` (
  `type_id` INT NOT NULL,
  `type_description` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`type_id`),
  UNIQUE INDEX `type_id_UNIQUE` (`type_id` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`positions`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`positions` (
  `position_id` INT UNSIGNED NOT NULL,
  `subdivision_name` VARCHAR(255) NOT NULL,
  `position_name` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`position_id`),
  UNIQUE INDEX `position_id_UNIQUE` (`position_id` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`appointments`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`appointments` (
  `appointment_id` INT UNSIGNED NOT NULL,
  `position_id` INT UNSIGNED NOT NULL,
  `people_id` INT UNSIGNED NOT NULL,
  `appointment_type_id` INT NOT NULL,
  `appointment_boss_id` INT UNSIGNED NULL,
  `date` DATE NOT NULL,
  `salary` DECIMAL(8,2) NOT NULL DEFAULT 0,
  `deleted_at` VARCHAR(45) NULL,
  PRIMARY KEY (`appointment_id`),
  INDEX `ap_types_idx` (`appointment_type_id` ASC),
  INDEX `ap_people_idx` (`people_id` ASC),
  INDEX `ap_position_idx` (`position_id` ASC),
  INDEX `ap_boss_idx` (`appointment_boss_id` ASC),
  CONSTRAINT `ap_types`
    FOREIGN KEY (`appointment_type_id`)
    REFERENCES `mydb`.`appointment_types` (`type_id`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE,
  CONSTRAINT `ap_people`
    FOREIGN KEY (`people_id`)
    REFERENCES `mydb`.`peoples` (`people_id`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE,
  CONSTRAINT `ap_position`
    FOREIGN KEY (`position_id`)
    REFERENCES `mydb`.`positions` (`position_id`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE,
  CONSTRAINT `ap_boss`
    FOREIGN KEY (`appointment_boss_id`)
    REFERENCES `mydb`.`appointments` (`appointment_id`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
