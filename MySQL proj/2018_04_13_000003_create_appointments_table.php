<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppointmentsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'appointments';

    /**
     * Run the migrations.
     * @table appointments
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('appointment_id');
            $table->unsignedInteger('position_id');
            $table->unsignedInteger('people_id');
            $table->integer('appointment_type_id');
            $table->unsignedInteger('appointment_boss_id')->nullable();
            $table->date('date');
            $table->decimal('salary', 8, 2)->default('0');

            $table->index(["appointment_boss_id"], 'ap_boss_idx');

            $table->index(["people_id"], 'ap_people_idx');

            $table->index(["position_id"], 'ap_position_idx');

            $table->index(["appointment_type_id"], 'ap_types_idx');
            $table->softDeletes();


            $table->foreign('appointment_type_id', 'ap_types_idx')
                ->references('type_id')->on('appointment_types')
                ->onDelete('no action')
                ->onUpdate('cascade');

            $table->foreign('people_id', 'ap_people_idx')
                ->references('people_id')->on('peoples')
                ->onDelete('no action')
                ->onUpdate('cascade');

            $table->foreign('position_id', 'ap_position_idx')
                ->references('position_id')->on('positions')
                ->onDelete('no action')
                ->onUpdate('cascade');

            $table->foreign('appointment_boss_id', 'ap_boss_idx')
                ->references('appointment_id')->on('appointments')
                ->onDelete('no action')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
