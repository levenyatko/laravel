
    $('#tree_block').tree({
        autoOpen: 0,
        selectable: false,
        closedIcon: '+',
        openedIcon: '-',
        dragAndDrop: true,
        onLoading: function(is_loading, node, $el){
            if(is_loading){
                $('#page_tree_loader').css("display", "flex");
            }else{
                $('#page_tree_loader').css("display", "none");
            }
        },
    });

    $('#tree_block').bind('tree.move', function(event) {
        event.preventDefault();

        $.confirm({
            title: 'Изменить назначение для сотрудника?',
            content: '',
            buttons: {
                confirm: {
                    text: 'да',
                    btnClass: 'btn btn-default',
                    action: function(){
                        $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            type: "POST",
                            url: $('#tree_block').data('move'),
                            data: {
                                moved_node: event.move_info.moved_node.id,
                                parent_node: event.move_info.target_node.id,
                                position: event.move_info.position,
                            },
                            success: function (data) {
                                event.move_info.do_move();
                            },
                            error: function (xhr, status, error) {
                                console.log(xhr.responseText);
                            }
                        });
                    }
                },
                cancel: {
                    text: 'нет',
                    btnClass: 'btn btn-primary',
                    keys: ['enter', 'shift'],
                    action: function(){
                    }
                }
            }
        });

    });

    $(document ).on( 'click', 'table tr.info a', function (e) {
        e.preventDefault();

        var $loader = $('#page_tree_loader');
        $loader.css("display", "flex");

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: "POST",
            url: $(this).attr('href'),
            data: { orderby: $(this).data('orderby'), order: $(this).data('order') },
            success: function (data) {
                $("#app-container").html(data);
                $loader.css("display", "none");

            },
            error: function (xhr, status, error) {
                console.log(xhr.responseText);
            }
        });
    });

    $(".spoiler-trigger").click(function() {
        $(this).parent().next().collapse('toggle');
        var $child = $(this).children();
        $child.toggleClass('fa-sort-desc');
        $child.toggleClass('fa-sort-asc');
    });

    $('#search_employee').on('submit', function () {

        var $form = $(this);
        var $loader = $('#page_tree_loader');

        $loader.css("display", "flex");

        $.ajax({
             headers: {
                'X-CSRF-TOKEN': $form.children('input[name="_token"]').val()
             },
             type:"POST",
             url:$form.attr('action'),
             data:$form.serialize(),
             success: function(data){
                 $("#app-container").html(data);
                 $loader.css("display", "none");
             },
             error: function (xhr, status, error) {
                 console.log(xhr.responseText);
             }
        });
        return false;
    });

    $('#search_employee').on('reset', function () {
        var $form = $(this);
        var $loader = $('#page_tree_loader');

        $loader.css("display", "flex");
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $form.children('input[name="_token"]').val()
            },
            type:"POST",
            url: $form.attr('action'),
            data: { clear: 'yes' },
            success: function(data){
                $("#app-container").html(data);
                $loader.css("display", "none");
                $form.find('input[type=text]').val('');
                $form.find('input[type=number]').val('');
                $form.find('input[type=date]').val('');
                $form.find('select').val('2');
            },
            error: function (xhr, status, error) {
                console.log(xhr.responseText);
            }
        });
    });

    $('#inputBoss').on('keyup',function(e){
        var $this = $(this), $result = $this.next().next(), route = '';

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: "POST",
            url:'http://localhost/laravel/public/bosses',
            data: { s: $this.val() },
            success: function (response) {
                $result.html(response);
                $result.removeClass('hidden');
            },
            error: function (xhr, status, error) {
                console.log(xhr.responseText);
            }
        });
    });

    $(document).on('click', "#search_boss p", function () {

        var code = $(this).data('code');

        $('#inputBoss').val($(this).text());

        $('#inputBossField').val(code);
        $(this).parent().addClass('hidden');
    });

    $('#inputPeople').on('keyup',function(e){
        var $this = $(this), $result = $this.next().next(), route = '';

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: "POST",
            url:'http://localhost/laravel/public/peoples',
            data: { s: $this.val() },
            success: function (response) {
                $result.html(response);
                $result.removeClass('hidden');
            },
            error: function (xhr, status, error) {
                console.log(xhr.responseText);
            }
        });
    });

    $(document).on('click', "#search_people p", function () {

        var code = $(this).data('code');
        var $group = $('.people-group'), $inputs = $('.people-group input');

        if(code == 'add'){
            $group.removeClass('hidden');
            $inputs.attr('required', true);
        }else{
            $group.addClass('hidden');
            $inputs.attr('required', false);
        }

        $('#inputPeople').val($(this).text());
        $('#inputPeopleField').val(code);
        $(this).parent().addClass('hidden');
    });

    $('#inputPosition').on('keyup',function(e){
        var $this = $(this), $result = $this.next().next(), route = '';

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: "POST",
            url:'http://localhost/laravel/public/positions',
            data: { s: $this.val() },
            success: function (response) {
                $result.html(response);
                $result.removeClass('hidden');
            },
            error: function (xhr, status, error) {
                console.log(xhr.responseText);
            }
        });
    });

    $(document).on('click', "#search_position p", function () {
        var code = $(this).data('code');
        var $group = $('.position-group'), $inputs = $('.position-group input');

        if(code == 'add'){
            $group.removeClass('hidden');
            $inputs.attr('required', true);
        }else{
            $group.addClass('hidden');
            $inputs.attr('required', false);
        }
        $('#inputPosition').val($(this).text());
        $('#inputPositionField').val(code);
        $(this).parent().addClass('hidden');
    });

    $(document).mouseup(function (e) {
        var container = $("#search_boss");
        if (container.has(e.target).length === 0){
            container.addClass('hidden');
        }
        var container = $("#search_people");
        if (container.has(e.target).length === 0){
            container.addClass('hidden');
        }
        var container = $("#search_position");
        if (container.has(e.target).length === 0){
            container.addClass('hidden');
        }
    });

    $(document).on('click', ".delete_form .del-row", function (e) {
        //e.preventDefault();
        var $this = $(this);

        $.confirm({
            title: 'Вы уверены?',
            content: '',
            buttons: {
                confirm: {
                    text: 'да',
                    btnClass: 'btn btn-default',
                    action: function(){
                        $this.parent().submit();
                    }
                },
                cancel: {
                    text: 'нет',
                    btnClass: 'btn btn-primary',
                    keys: ['enter', 'shift'],
                    action: function(){
                    }
                }
            }
        });

    });

    function readURL(input) {
        console.log(input);
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#image_show').attr('src', e.target.result);
                $('#image_fl').val(1);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#image_input").change(function() {
        readURL(this);
    });