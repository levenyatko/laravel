<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use BenAllfree\LaravelStaplerImages\AttachmentTrait;

class Peoples extends Model // Люди
{
    public $timestamps = false;
    use AttachmentTrait;

}
