<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Appointments;

class AppointmentsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        Appointments::deleted(function($model){
            $subordinates = Appointments::where('appointment_boss_id','=', $model->appointment_id)->count();
            if($subordinates > 0){
                $boss_id = $model->getBossIdAttribute();

                Appointments::where('appointment_boss_id','=', $model->appointment_id)
                    ->update(['appointment_boss_id' => $boss_id ]);
            }
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
