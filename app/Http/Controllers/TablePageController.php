<?php

namespace App\Http\Controllers;

use App\Appointments;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TablePageController extends Controller
{
    private $table_headers = array(
        'appointment_id' => '#',
        'photo' => 'Фото',
        'first_name' => 'Имя',
        'last_name' => 'Фамилия',
        'isMale' => 'Пол',
        'subdivision_name' => 'Подразделение',
        'position_name' => 'Должность',
        'ap_date' => 'Дата назначения',
        'salary' => 'Зар. плата');

    private $orderby;
    private $order;

    private function get_table_header_array(Request $request, $set_session = 0){
        $order_array = array();

        if($request->has('orderby')){
            $this->orderby = $request->get('orderby');
            if($request->has('order')){
                $this->order = $request->get('order');
            }else{
                $this->order = 'asc';
            }
        }else{
            if($request->session()->has('lar_test_orderby')){
                $this->orderby = $request->session()->get('lar_test_orderby');
                if($request->session()->has('lar_test_order')){
                    $this->order = $request->session()->get('lar_test_order');
                }else{
                    $this->order = 'asc';
                }
            }else{
                $this->orderby = 'appointment_id';
                $this->order = 'asc';
            }
        }

        if($set_session){
            session(['lar_test_orderby' => $this->orderby]);
            session(['lar_test_order' => $this->order]);
        }

        foreach ($this->table_headers as $key => $title){
            $order_array[$key]['name'] = $title;
            if($this->orderby == $key){
                if($this->order) {
                    if($this->order == 'asc'){
                        $order_array[$key]['order_class'] = 'fa-sort-asc';
                        $order_array[$key]['order'] = 'desc';
                    }else{
                        $order_array[$key]['order_class'] = 'fa-sort-desc';
                        $order_array[$key]['order'] = 'asc';
                    }
                }else{
                    $order_array[$key]['order_class'] = 'fa-sort-asc';
                    $order_array[$key]['order'] = 'desc';
                }
            }
            else{
                $order_array[$key]['order_class'] = 'fa-unsorted';
                $order_array[$key]['order'] = 'asc';
            }
        }

        return $order_array;
    }

    public function get_workers_data(Request $request){

        $per_page = 200;
        $table = DB::table('current_workers');

        $data = null;

        // Поиск
        $where_arr = array();

        if($request->has('clear')){
            session()->forget('lar_test_idfield');
            session()->forget('lar_test_fname');
            session()->forget('lar_test_lname');
            session()->forget('lar_test_s');
            session()->forget('lar_test_subdiv');
            session()->forget('lar_test_pos');
            session()->forget('lar_test_date');
            session()->forget('lar_test_sal');
        }

        if($request->has('idfield') && $request->get('idfield')){
            $where_arr[] = array('appointment_id', '=', $request->get('idfield'));
            session(['lar_test_idfield' => $request->get('idfield')]);
        }else{
            if($request->session()->has('lar_test_idfield')){
                $where_arr[] = array('appointment_id', '=', $request->session()->get('lar_test_idfield'));
            }
        }

        if($request->has('first_name') && $request->get('first_name')){
            $where_arr[] = array('first_name', 'LIKE', '%'.$request->get('first_name').'%');
            session(['lar_test_fname' => $request->get('first_name')]);
        }else{
            if($request->session()->has('lar_test_fname')){
                $where_arr[] = array('first_name', 'LIKE', '%'.$request->session()->get('lar_test_fname').'%');
            }
        }

        if($request->has('last_name') && $request->get('last_name')){
            $where_arr[] = array('last_name', 'LIKE', '%'.$request->get('last_name').'%');
            session(['lar_test_lname' => $request->get('last_name')]);
        }else{
            if($request->session()->has('lar_test_lname')){
                $where_arr[] = array('last_name', 'LIKE', '%'.$request->session()->get('lar_test_lname').'%');
            }
        }

        if($request->has('sex')){
            $s = $request->get('sex');
            if($s == '1'){
                $where_arr[] = array('isMale', '=', '1');
                session(['lar_test_s' => '1']);
            }elseif($s == '0'){
                $where_arr[] = array('isMale', '=', '0');
                session(['lar_test_s' => '0']);
            }
        }else{
            if($request->session()->has('lar_test_s')){
                $where_arr[] = array('isMale', '=', $request->session()->get('lar_test_s'));
            }
        }

        if($request->has('subdiv') && $request->get('subdiv')){
            $where_arr[] = array('subdivision_name', 'LIKE', '%'.$request->get('subdiv').'%');
            session(['lar_test_subdiv' => $request->get('subdiv')]);
        }else{
            if($request->session()->has('lar_test_subdiv')){
                $where_arr[] = array('subdivision_name', 'LIKE', '%'.$request->session()->get('lar_test_subdiv').'%');
            }
        }

        if($request->has('pos') && $request->get('pos')){
            $where_arr[] = array('position_name', 'LIKE', '%'.$request->get('pos').'%');
            session(['lar_test_pos' => $request->get('pos')]);
        }else{
            if($request->session()->has('lar_test_pos')){
                $where_arr[] = array('position_name', 'LIKE', '%'.$request->session()->get('lar_test_pos').'%');
            }
        }

        if($request->has('date') && $request->get('date')){
            $where_arr[] = array('ap_date', '=', $request->get('date'));
            session(['lar_test_date' => $request->get('date') ]);
        }else{
            if($request->session()->has('lar_test_date')){
                $where_arr[] = array('ap_date', '=', $request->session()->get('lar_test_date') );
            }
        }

        if($request->has('salary') && $request->get('salary')){
            $where_arr[] = array('salary', '=', $request->get('salary'));
            session(['lar_test_sal' => $request->get('salary') ]);
        }else{
            if($request->session()->has('lar_test_sal')){
                $where_arr[] = array('salary', '=', $request->session()->get('lar_test_sal'));
            }
        }

        if(count($where_arr) > 0){
            $table->where($where_arr);
        }

        // сортировка
        $table->orderBy($this->orderby, $this->order);

        $data = $table->paginate( $per_page);

        return $data;
    }

    public function index(Request $request)
    {
        $order_array = $this->get_table_header_array($request);
        $data = $this->get_workers_data($request);

        return view('table.index', [
            'data' => $data,
            'paginate' => $data->render(),
            'order_arr' => $order_array,
        ]);
    }

    public function ajaxIndex(Request $request)
    {
        $order_array = $this->get_table_header_array($request, 1);
        $data = $this->get_workers_data($request);

        return view('table.table', [
            'data' => $data,
            'paginate' => $data->render(),
            'order_arr' => $order_array,
        ]);
    }

}
