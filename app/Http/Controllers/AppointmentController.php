<?php

namespace App\Http\Controllers;

use App\Appointments;
use App\AppointmentTypes;
use App\Peoples;
use App\Positions;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use BenAllfree\LaravelStaplerImages\Image;

class AppointmentController extends Controller
{
    private $table_headers = array(
        'appointment_id' => '#',
        'photo' => 'Фото',
        'first_name' => 'Имя',
        'last_name' => 'Фамилия',
        'isMale' => 'Пол',
        'subdivision_name' => 'Подразделение',
        'position_name' => 'Должность',
        'appointment_type_id' => 'Тип назначения',
        'date' => 'Дата назначения',
        'salary' => 'Зар. плата');

    private $orderby;
    private $order;

    private function get_table_header_array(Request $request, $set_session = 0){
        $order_array = array();

        if($request->has('orderby')){
            $this->orderby = $request->get('orderby');
            if($request->has('order')){
                $this->order = $request->get('order');
            }else{
                $this->order = 'asc';
            }
        }else{
            if($request->session()->has('lar_test_orderby1')){
                $this->orderby = $request->session()->get('lar_test_orderby1');
                if($request->session()->has('lar_test_order1')){
                    $this->order = $request->session()->get('lar_test_order1');
                }else{
                    $this->order = 'asc';
                }
            }else{
                $this->orderby = 'appointment_id';
                $this->order = 'asc';
            }
        }

        if($set_session){
            session(['lar_test_orderby1' => $this->orderby]);
            session(['lar_test_order1' => $this->order]);
        }

        foreach ($this->table_headers as $key => $title){
            $order_array[$key]['name'] = $title;
            if($this->orderby == $key){
                if($this->order) {
                    if($this->order == 'asc'){
                        $order_array[$key]['order_class'] = 'fa-sort-asc';
                        $order_array[$key]['order'] = 'desc';
                    }else{
                        $order_array[$key]['order_class'] = 'fa-sort-desc';
                        $order_array[$key]['order'] = 'asc';
                    }
                }else{
                    $order_array[$key]['order_class'] = 'fa-sort-asc';
                    $order_array[$key]['order'] = 'desc';
                }
            }
            else{
                $order_array[$key]['order_class'] = 'fa-unsorted';
                $order_array[$key]['order'] = 'asc';
            }
        }

        return $order_array;
    }

    /**
     * @param Request $request
     * @return null
     */
    public function get_workers_data(Request $request){

        $per_page = 200;

        $data = null;

        // сортировка
        $table = DB::table('appointments')->whereNull('deleted_at')
                        ->join('peoples', 'appointments.people_id', '=', 'peoples.people_id')
                        ->join('positions', 'appointments.position_id', '=', 'positions.position_id')
                        ->join('appointment_types', 'appointments.appointment_type_id', '=', 'appointment_types.type_id');

        $table->orderBy($this->orderby, $this->order);

        $data = $table->paginate( $per_page );
        $data->withPath(route('appointments.index'));

        return $data;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $order_array = $this->get_table_header_array($request);
        $data = $this->get_workers_data($request);

        return view('page.index', [
            'data' => $data,
            'paginate' => $data->render(),
            'order_arr' => $order_array,
        ]);
    }

    public function ajaxIndex(Request $request)
    {
        $order_array = $this->get_table_header_array($request, 1);
        $data = $this->get_workers_data($request);

        return view('page.table', [
            'data' => $data,
            'paginate' => $data->render(),
            'order_arr' => $order_array,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $types_arr = array();
        foreach (AppointmentTypes::all() as $row){
            $types_arr[$row->type_id] = $row->type_description;
        }

        return view('page.create', [
            'types_arr' => $types_arr,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
                                                    'people' => 'required',
                                                    'first_name' => 'required_if:people,add|max:255',
                                                    'last_name' => 'required_if:people,add|max:255',
                                                    'sex' => 'required|in:1,0',
                                                    'position' => 'required',
                                                    'subdiv' => 'required_if:position,add|max:255',
                                                    'pos' => 'required_if:position,add|max:255',
                                                    'type' => 'required|in:-1,1,0',
                                                    //'boss' => 'required|numeric',
                                                    'date' => 'required|date_format:Y-m-d',
                                                    'salary' => 'required|numeric',
                                                    'avatar_image' => 'required'
                                                ]);
        $people_id = -1;
        $position_id = -1;

        $people = null;
        $position = null;

        if($request->get('people') == 'add'){

            $new_people = new Peoples;
            $new_people->first_name = $request->get('first_name');
            $new_people->last_name = $request->get('last_name');
            $new_people->isMale = $request->get('sex');

            if ($request->hasFile('avatar_image') && $request->file('avatar_image')->isValid()) {
                $path = $request->avatar_image->store('images');
                $image = Image::from_url(config('laravel-stapler.images.la_path').$path );
                $new_people->avatar_image_id = $image->id;
            }

            $new_people->save();

            $people_id = $new_people->id;
            $people = $new_people;
        }else{
            $people_id = $request->get('people');
            $people = Peoples::where('people_id',$people_id)->first();
        }

        if($request->get('position') == 'add'){

            $new_position = new Positions;
            $new_position->subdivision_name = $request->get('subdiv');
            $new_position->position_name = $request->get('pos');
            $new_position->save();

            $position_id = $new_position->id;
            $position = $new_position;
        }else{
            $position_id = $request->get('position');
            $position = Positions::where('position_id',$position_id)->first();
        }

        $new_row = new Appointments;
        $new_row->position_id = $position_id;
        $new_row->people_id = $people_id;
        $new_row->appointment_type_id = $request->get('type');
        $new_row->appointment_boss_id = $request->get('boss');
        $new_row->date = $request->get('date');
        $new_row->salary = $request->get('salary');
        $new_row->save();

        $request->session()->flash('success', 'Запись добавлена');

        return redirect()->route('appointments.show', $new_row->id );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $appointment = Appointments::where('appointment_id',$id)->first();
        if(!$appointment ) {
            abort(404);
        }
        $people = Peoples::where('people_id',$appointment->people_id)->first();
        $position = Positions::where('position_id',$appointment->position_id)->first();

        $boss = null;
        $boss_id = null;
        if($appointment->appointment_boss_id) {
            $boss_id = $appointment->boss_id;
            if($boss_id ) {
                $boss_query = DB::table('appointments')
                    ->join('peoples', 'appointments.people_id', '=', 'peoples.people_id')
                    ->join('positions', 'appointments.position_id', '=', 'positions.position_id')
                    ->where('appointments.appointment_id', $boss_id)
                    ->first();

                $boss = $boss_query->appointment_id . '; ' . $boss_query->first_name . ' ' . $boss_query->last_name . '; ' . $boss_query->subdivision_name . '; ' . $boss_query->position_name;
            }
        }

        return view('page.show', [
            'item' => $appointment,
            'people' => $people,
            'position' => $position,
            'boss' => $boss,
            'boss_url' => route('appointments.show', ['appointment' => $boss_id ])
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $types_arr = array();
        foreach (AppointmentTypes::all() as $row){
            $types_arr[$row->type_id] = $row->type_description;
        }
        $item = Appointments::where('appointment_id', $id)->first();

        $people = Peoples::where('people_id',$item->people_id)->first();
        $people_str = $people->people_id.'; '.$people->first_name.' '.$people->last_name;

        $position = Positions::where('position_id',$item->position_id)->first();
        $position_str = $position->position_id.'; '.$position->subdivision_name.'; '.$position->position_name;

        $boss = null;
        if($item->appointment_boss_id) {
            $boss_query = DB::table('appointments')
                ->join('peoples', 'appointments.people_id', '=', 'peoples.people_id')
                ->join('positions', 'appointments.position_id', '=', 'positions.position_id')
                ->where('appointments.appointment_id', $item->appointment_boss_id)
                ->first();

            $boss = $boss_query->appointment_id.'; '.$boss_query->first_name.' '.$boss_query->last_name.'; '.$boss_query->subdivision_name.'; '.$boss_query->position_name;
        }


        return view('page.edit', [
            'types_arr' => $types_arr,
            'people_string' => $people_str,
            'position_string' => $position_str,
            'boss_string' => $boss,
            'item' => $item,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'people' => 'required',
            'first_name' => 'required_if:people,add|max:255',
            'last_name' => 'required_if:people,add|max:255',
            'sex' => 'required|in:1,0',
            'position' => 'required',
            'subdiv' => 'required_if:position,add|max:255',
            'pos' => 'required_if:position,add|max:255',
            'type' => 'required|in:-1,1,0',
            //'boss' => 'required|numeric',
            'date' => 'required|date_format:Y-m-d',
            'salary' => 'required|numeric'
        ]);
        $people_id = -1;
        $position_id = -1;

        $people = null;
        $position = null;

        if($request->get('people') == 'add'){

            $new_people = new Peoples;
            $new_people->first_name = $request->get('first_name');
            $new_people->last_name = $request->get('last_name');
            $new_people->isMale = $request->get('sex');
            if ($request->hasFile('avatar_image') && $request->file('avatar_image')->isValid()) {
                $path = $request->avatar_image->store('images');
                $image = Image::from_url(config('laravel-stapler.images.la_path').$path );
                $new_people->avatar_image_id = $image->id;
            }
            $new_people->save();

            $people_id = $new_people->id;
            $people = $new_people;
        }else{
            $people_id = $request->get('people');
            $people = Peoples::where('people_id',$people_id)->first();
        }

        if($request->get('position') == 'add'){

            $new_position = new Positions;
            $new_position->subdivision_name = $request->get('subdiv');
            $new_position->position_name = $request->get('pos');
            $new_position->save();

            $position_id = $new_position->id;
            $position = $new_position;
        }else{
            $position_id = $request->get('position');
            $position = Positions::where('position_id',$position_id)->first();
        }

        $item = Appointments::where('appointment_id', $id)->update([
                                                                    'position_id' => $position_id,
                                                                    'people_id' => $people_id,
                                                                    'appointment_type_id' => $request->get('type'),
                                                                    'appointment_boss_id' => $request->get('boss'),
                                                                    'date' => $request->get('date'),
                                                                    'salary' => $request->get('salary'),
                                                                  ]);


        $request->session()->flash('success', 'Запись изменена');

        return redirect()->route('appointments.show', $id );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $posts = Appointments::where('appointment_id', '=', $id)->get();
        foreach ( $posts as $post )
        {
            $post->delete();
        }
       //Appointments::where('appointment_id','=', $id)->delete();

        Session()->flash('success', 'Запись удалена');
        return redirect()->route('appointments.index');
    }
}
