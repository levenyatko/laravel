<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SearchController extends Controller
{
    public function getBossList(Request $request){
        $res = array();

        $s = $request->get('s');

        $table = DB::table('current_workers');
        $table->where('appointment_id', 'LIKE', $s.'%')
              ->orWhere('first_name', 'LIKE', '%'.$s.'%')
              ->orWhere('last_name', 'LIKE', '%'.$s.'%')
              ->orWhere('subdivision_name', 'LIKE', '%'.$s.'%')
              ->orWhere('position_name', 'LIKE', '%'.$s.'%');

        $data = $table->simplePaginate( 5 );

       foreach ( $data as $item ) {
            $res[$item->appointment_id] = $item->appointment_id.'; '.$item->first_name.' '.$item->last_name.'; '.$item->subdivision_name.'; '.$item->position_name;
        }

        return view('layouts.search_res', [
            'add' => 0,
            'data' => $res,
        ]);
    }

    public function getPeoplesList(Request $request){
        $res = array();

        $s = $request->get('s');

        $table = DB::table('peoples');
        $table->where('first_name', 'LIKE', '%'.$s.'%')
            ->orWhere('last_name', 'LIKE', '%'.$s.'%');
        $table->orderBy('first_name','asc')->orderBy('last_name','asc');
        $data = $table->simplePaginate( 5 );

        foreach ( $data as $item ) {
            $res[$item->people_id] = $item->people_id.'; '.$item->first_name.' '.$item->last_name;
        }

        return view('layouts.search_res', [
            'add' => 1,
            'data' => $res,
        ]);
    }

    public function getPositionsList(Request $request){
        $res = array();

        $s = $request->get('s');

        $table = DB::table('positions');
        $table->where('subdivision_name', 'LIKE', '%'.$s.'%')
            ->orWhere('position_name', 'LIKE', '%'.$s.'%');
        $table->orderBy('subdivision_name','asc')->orderBy('position_name','asc');
        $data = $table->simplePaginate( 5 );

        foreach ( $data as $item ) {
            $res[$item->position_id] = $item->position_id.'; '.$item->subdivision_name.'; '.$item->position_name;
        }

        return view('layouts.search_res', [
            'add' => 1,
            'data' => $res,
        ]);
    }

}
