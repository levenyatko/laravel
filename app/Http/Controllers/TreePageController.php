<?php

namespace App\Http\Controllers;

use App\Appointments;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TreePageController extends Controller
{
    public function index()
    {
        return view('tree.index' );
    }

    private function getWorkersTreeArray($id = Null){
        $WorkersTreeArray = array();

        $query_workers_level = DB::table('current_workers')->where('appointment_boss_id', '=', $id)->get();
        foreach ($query_workers_level as $row) {

            $label = $row->last_name.' '.$row->first_name;
            $label = $label.'; '.$row->subdivision_name.'; '.$row->position_name;
            $label = $label.'; с '.$row->ap_date;
            $label = $label.'; зп '.$row->salary;

            $have_childrens = true;
            /*if($id){
                $query_workers_level = DB::table('current_workers')->where('appointment_boss_id', '=', $row->position_id)->count();
                if(!$query_workers_level){
                    $have_childrens = false;
                }
            }*/

            $WorkersTreeArray[] =array(
                'label'     =>  $label,
                'id'        =>  $row->appointment_id,
                'children'  =>  array(),
                'load_on_demand' => ($id)?(($have_childrens)?true:false):false,
            );

        }

        return $WorkersTreeArray;
    }

    public function data(Request $request)
    {
        //?node=23 - ссылка на загрузку дочерних для 23 id
        $parent_node = $request->query('node');

        $tree_arr = array();

        if($parent_node){
            $tree_arr = $this->getWorkersTreeArray($parent_node);
        }else{
            $tree_arr = $this->getWorkersTreeArray();

            $tree_count = count($tree_arr);

            for ( $i=0 ; $i < $tree_count; $i++ ){
                $tree_arr[$i]['children'] = $this->getWorkersTreeArray($tree_arr[$i]['id']);
            }
        }

        return response()->json( $tree_arr );
    }

    public function move(Request $request)
    {
      if($request->has('moved_node') && $request->has('parent_node') && $request->has('position')){
          if(strcmp($request->get('position'),'inside') === 0){
            $mowed = Appointments::find($request->get('moved_node'));
            if($mowed){
                $parent = Appointments::find($request->get('parent_node'));
                if($parent){
                    $mowed->appointment_boss_id = $parent->appointment_id;
                    $mowed->save();
                    return 1;
                }
            }
          }else{ // after & before
              $mowed = Appointments::find($request->get('moved_node'));
              if($mowed){
                  $neigh = Appointments::find($request->get('parent_node'));
                  if($neigh){
                      $mowed->appointment_boss_id = $neigh->appointment_boss_id;
                      $mowed->save();
                      return 1;
                  }
              }
          }
      }

      return -1;
    }
}
