<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Appointments extends Model //назначения
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $primaryKey = 'appointment_id';

    public $timestamps = false;

    private function getBoss($id){

        $temp = Appointments::withTrashed()->where('appointment_id', $id)->first();
        if($temp->trashed()){
            $boss_id = $this->getBoss($temp->appointment_boss_id);
        }else{
            $boss_id = $id;
        }
        return $boss_id ;
    }

    public function getBossIdAttribute()
    {
        if($this->attributes['appointment_boss_id']){
            $id = $this->getBoss($this->attributes['appointment_boss_id']);
            return $id;
        }
        return null;
    }
}
