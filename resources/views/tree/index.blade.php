@extends('app')

@section('title','Список работников в виде дерева')

@section('content')
<div class="container-fluid">
    <div id="tree_block" data-url="{!! route('tree_data') !!}" data-move="{!! route('move_node') !!}"></div>
</div>

@endsection