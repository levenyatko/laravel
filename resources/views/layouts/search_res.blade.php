@if($add)
    <p data-code="add">Новый...</p>
@endif
@if(count($data) > 0)
    @foreach($data as $key => $val)
        <p data-code="{{$key}}">{{$val}}</p>
    @endforeach
@endif