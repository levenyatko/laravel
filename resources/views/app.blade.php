@include('layouts.header')

        <div class="container">
            <div class="row">
                    <div class="panel panel-default">
                        <h2 class="panel-heading">@yield('title')</h2>
                        @if(Session::has('success'))
                            <div class="alert alert-success alert-in-panel">
                                {{Session::get('success')}}
                            </div>
                        @endif
                        <div class="panel-body">
                            @yield('content')
                        </div>
                    </div>
            </div>
        </div>

        <div class="load_container" id="page_tree_loader"><div class="loader"></div></div>

@include('layouts.footer')