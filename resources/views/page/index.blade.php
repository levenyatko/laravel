@extends('app')

@section('title','Список назначений работников')

@section('content')
<div class="container">
    <p>
        <a href="{{ route('appointments.create') }}" class="btn btn-primary">Добавить запись</a>
    </p>
</div>
<div  id="app-container">
    @include('page.table')
</div>
@endsection