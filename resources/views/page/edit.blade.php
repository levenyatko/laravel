@extends('app')

@section('title','Редактирование назначения #'.$item->appointment_id)

@section('content')
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="container-fluid" id="app-container">
        <p class="text-info">*все поля являются обязательными для заполнения</p>
        {!! Form::open(['class'=>'form-horizontal', 'route' => ['appointments.update',$item->appointment_id], 'method' => 'put','files'=>'true']) !!}

        <div class="form-group">
            {!! Form::label('inputPeople', 'Сотрудник',['class'=>'col-sm-2']) !!}
            <div class="col-sm-4 search-field">
                {!! Form::text('', $people_string, ['class' => 'form-control search-input','autocomplete'=>'off', 'id'=>'inputPeople', 'required' => 'required'])!!}
                {!! Form::hidden('people', $item->people_id, ['id'=>'inputPeopleField', 'required' => 'required'])!!}
                <div class="search_data hidden" id="search_people"></div>
            </div>
        </div>

        <div class="form-group people-group hidden">
            {!! Form::label('inputFName', 'Имя',['class'=>'col-sm-2']) !!}
            <div class="col-sm-3">
                {!! Form::text('first_name', null, ['class' => 'form-control', 'id'=>'inputFName'])!!}
            </div>
        </div>
        <div class="form-group people-group hidden">
            {!! Form::label('inputLName', 'Фамилия',['class'=>'col-sm-2']) !!}
            <div class="col-sm-3">
                {!! Form::text('last_name',  null , ['class' => 'form-control', 'id'=>'inputLName'])!!}
            </div>
        </div>
        <div class="form-group people-group hidden">
            {!! Form::label('inputSex', 'Пол',['class'=>'col-sm-2']) !!}
            <div class="col-sm-2">
                {!! Form::select('sex', ['1' => 'М', '0' => 'Ж'], 1, ['class' => 'form-control','id' => 'inputSex'])!!}
            </div>
        </div>
        <div class="form-group people-group hidden">
            {!! Form::label('inputPhoto', 'Фото',['class'=>'col-sm-2']) !!}
            <div class="col-sm-3">
                <img src="{{asset('img/images.png')}}" id="image_show" />
                {!! Form::file('avatar_image',['id'=>'image_input']) !!}
                {!! Form::hidden('avatar_changed',0,['id'=>'image_fl']) !!}
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('inputPosition', 'Должность',['class'=>'col-sm-2']) !!}
            <div class="col-sm-4 search-field">
                {!! Form::text('', $position_string, ['class' => 'form-control search-input','autocomplete'=>'off', 'id'=>'inputPosition', 'required' => 'required'])!!}
                {!! Form::hidden('position', $item->position_id, ['id'=>'inputPositionField', 'required' => 'required'])!!}
                <div class="search_data hidden" id="search_position"></div>
            </div>
        </div>

        <div class="form-group position-group hidden">
            {!! Form::label('inputSubdiv', 'Подразделение',['class'=>'col-sm-2']) !!}
            <div class="col-sm-3">
                {!! Form::text('subdiv', session('lar_test_subdiv'), ['class' => 'form-control', 'id'=>'inputSubdiv'])!!}
            </div>
        </div>
        <div class="form-group position-group hidden">
            {!! Form::label('inputPos', 'Должность',['class'=>'col-sm-2']) !!}
            <div class="col-sm-3">
                {!! Form::text('pos', session('lar_test_pos'), ['class' => 'form-control', 'id'=>'inputPos'])!!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('inputType', 'Тип назначения',['class'=>'col-sm-2']) !!}
            <div class="col-sm-2">
                {!! Form::select('type', $types_arr, $item->appointment_type_id, ['class' => 'form-control','id' => 'inputType'])!!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('inputBoss', 'Начальник',['class'=>'col-sm-2']) !!}
            <div class="col-sm-4 search-field">
                {!! Form::text('boss_text', $boss_string, ['class' => 'form-control search-input','autocomplete'=>'off', 'id'=>'inputBoss'])!!}
                {!! Form::hidden('boss', $item->appointment_boss_id, ['id'=>'inputBossField', 'required' => 'required'])!!}
                <div class="search_data hidden" id="search_boss"></div>
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('inputDate', 'Дата назначения',['class'=>'col-sm-2']) !!}
            <div class="col-sm-3">
                {!! Form::date('date', $item->date, ['class' => 'form-control', 'id'=>'inputDate', 'required' => 'required'])!!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('inputSalary', 'Зар. плата',['class'=>'col-sm-2']) !!}
            <div class="col-sm-3">
                {!! Form::number('salary', $item->salary, ['class' => 'form-control', 'id'=>'inputSalary', 'required' => 'required'])!!}
            </div>
        </div>

        <div class="buttons-goup">
            <div class="col-sm-3 col-sm-offset-2">
                {!! Form::submit('Сохранить', ['class' => 'btn btn-primary'])!!}
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@endsection