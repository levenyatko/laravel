@extends('app')

@section('title','Просмотр назначения #'.$item->appointment_id)

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2"><p><strong>Фото</strong></p></div>
            <div><p>
                @if($people->avatar_image_id)
                    <img src="{{ asset($people->avatar_image->url('thumb')) }}">
                @else
                        <img src="{{asset('img/images.png')}}" />
                @endif
            </p></div>
        </div>
        <div class="row">
            <div class="col-sm-2"><p><strong>Имя</strong></p></div>
            <div><p>{{$people->first_name}}</p></div>
        </div>
        <div class="row">
            <div class="col-sm-2"><p><strong>Фамилия</strong></p></div>
            <div><p>{{$people->last_name}}</p></div>
        </div>
        <div class="row">
            <div class="col-sm-2"><p><strong>Пол</strong></p></div>
            <div><p>{{ ($people->isMale) ? 'М' : 'Ж' }}</p></div>
        </div>
        <div class="row">
            <div class="col-sm-2"><p><strong>Подразделение</strong></p></div>
            <div><p>{{$position->subdivision_name}}</p></div>
        </div>
        <div class="row">
            <div class="col-sm-2"><p><strong>Должность</strong></p></div>
            <div><p>{{$position->position_name}}</p></div>
        </div>
        <div class="row">
            <div class="col-sm-2"><p><strong>Тип назначения</strong></p></div>
            <div><p>{{ ($item->appointment_type_id) ? 'прием' : (($item->appointment_type_id == 0)? 'перевод' : 'увольнение') }}</p></div>
        </div>
        <div class="row">
            <div class="col-sm-2"><p><strong>Дата назначения</strong></p></div>
            <div><p>{{ Carbon\Carbon::parse($item->date)->format('d.m.Y') }}</p></div>
        </div>
        <div class="row">
            <div class="col-sm-2"><p><strong>Зар. плата</strong></p></div>
            <div><p>{{ $item->salary }}</p></div>
        </div>
        @if($boss)
            <div class="row">
                <div class="col-sm-2"><p><strong>Начальник</strong></p></div>
                <div><p><a href="{{ $boss_url }}">{{ $boss }}</a></p></div>
            </div>
        @endif
        <div class="row">
            <a href="{{route('appointments.edit',['appointment' => $item->appointment_id] )}}" class=" col-sm-offset-2 btn btn-primary">Редактировать</a>
            {!! Form::open(['route' => ['appointments.destroy', $item->appointment_id], 'method' => 'delete', 'class' => 'delete_form']) !!}
            {!! Form::button('Удалить', ['class' => 'btn btn-default del-row'])!!}
            {!! Form::close() !!}
        </div>
    </div>
@endsection