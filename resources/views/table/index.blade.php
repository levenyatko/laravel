@extends('app')

@section('title','Список работников в виде таблицы')

@section('content')
<div class="container-fluid">

    <div class="panel panel-search">
        <div class="panel-heading">
            <h4 class="form-title spoiler-trigger pointer" data-toggle="collapse">Поиск сотрудников <i class="fa fa-sort-desc"></i></h4>
        </div>
        <div class="panel-collapse collapse out">
            <div class="panel-body">
                {!! Form::open(['class'=>'form-horizontal', 'id'=>'search_employee', 'route' => 'workers_list']) !!}
                    <div class="form-group">
                        {!! Form::label('inputId', '#',['class'=>'col-sm-2']) !!}
                        <div class="col-sm-2">
                            {!! Form::number('idfield', session('lar_test_idfield') , ['class' => 'form-control', 'id'=>'inputId'])!!}
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('inputFName', 'Имя',['class'=>'col-sm-2']) !!}
                        <div class="col-sm-3">
                            {!! Form::text('first_name',  session('lar_test_fname') , ['class' => 'form-control', 'id'=>'inputFName'])!!}
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('inputLName', 'Фамилия',['class'=>'col-sm-2']) !!}
                        <div class="col-sm-3">
                            {!! Form::text('last_name',  session('lar_test_lname') , ['class' => 'form-control', 'id'=>'inputLName'])!!}
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('inputSex', 'Пол',['class'=>'col-sm-2']) !!}
                        <div class="col-sm-2">
                            {!! Form::select('sex', ['2' => 'Любой', '1' => 'М', '0' => 'Ж'], session('lar_test_s', 2), ['class' => 'form-control','id' => 'inputSex'])!!}
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('inputSubdiv', 'Подразделение',['class'=>'col-sm-2']) !!}
                        <div class="col-sm-3">
                            {!! Form::text('subdiv', session('lar_test_subdiv'), ['class' => 'form-control', 'id'=>'inputSubdiv'])!!}
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('inputPos', 'Должность',['class'=>'col-sm-2']) !!}
                        <div class="col-sm-3">
                            {!! Form::text('pos', session('lar_test_pos'), ['class' => 'form-control', 'id'=>'inputPos'])!!}
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('inputDate', 'Дата назначения',['class'=>'col-sm-2']) !!}
                        <div class="col-sm-3">
                            {!! Form::date('date', session('lar_test_date'), ['class' => 'form-control', 'id'=>'inputDate'])!!}
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('inputSalary', 'Зар. плата',['class'=>'col-sm-2']) !!}
                        <div class="col-sm-3">
                            {!! Form::number('salary', session('lar_test_sal'), ['class' => 'form-control', 'id'=>'inputSalary'])!!}
                        </div>
                    </div>
                    <div class="buttons-goup">
                        <div class="col-sm-3 col-sm-offset-2">
                            {!! Form::submit('Поиск', ['class' => 'btn btn-primary'])!!}
                            {!! Form::reset('Сброс', ['class' => 'btn btn-link', 'id' => 'reset_search_button']) !!}
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>


</div>
<div  id="app-container">
    @include('table.table')
</div>
@endsection