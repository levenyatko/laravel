<div class="container-fluid" id="ajax-container">
  @if(count($data) > 0)
  <table class="table table-hover">
    <thead>
    <tr class="info">
        @foreach($order_arr as $key => $row)
            <td>
                {{ $row['name'] }}
                @if($key != 'photo')
                    <a href="{{ route('workers_list') }}" data-orderby = '{{ $key }}'  data-order = '{{$row['order']}}'><i class="fa {{$row['order_class']}}"></i></a>
                @endif
            </td>
        @endforeach
        <td></td>
    </tr>
    </thead>
    <tbody>
    <?php ?>
    @foreach($data as $key => $employee)
      <tr>
          <td>{{ $employee->appointment_id }}</td>
          <td>
              @if($employee->avatar_image_id)
                  <?php $image =Image::find($employee->avatar_image_id);  ?>
                  <a href="{{ $image->original_file_name}}" target="_blank">
                      <img src="{{ asset($image->url('tiny')) }}">
                  </a>
              @else
                  <img src="{{asset('img/images.png')}}" />
              @endif
          </td>
          <td>{{ $employee->first_name }}</td>
          <td>{{ $employee->last_name }}</td>
          <td>{{ ($employee->isMale) ? 'М' : 'Ж' }}</td>
          <td>{{ $employee->subdivision_name }}</td>
          <td>{{ $employee->position_name }}</td>
          <td>{{ Carbon\Carbon::parse($employee->ap_date)->format('d.m.Y') }}</td>
          <td>{{ $employee->salary }}</td>
          <td>
              <a class="fa fa fa-eye" title="просмотр" aria-hidden="true" href="{{route('appointments.show',['appointment' => $employee->appointment_id] )}}"></a>
              <a class="fa fa-pencil-square-o" title="изменить" aria-hidden="true" href="{{route('appointments.edit',['appointment' => $employee->appointment_id] )}}"></a>
              {!! Form::open(['route' => ['appointments.destroy', $employee->appointment_id], 'method' => 'delete', 'class' => 'delete_form']) !!}
              {!! Form::button('', ['class' => 'fa fa-trash-o del-row', 'title' => 'удалить'])!!}
              {!! Form::close() !!}
          </td>
      </tr>
    @endforeach
    </tbody>
  </table>

    <div >
        {{ $paginate }}
    </div>
  @else
        <p class="bg-primary text-center info-text">По Вашему запросу ничего не найдено</p>
  @endif
</div>