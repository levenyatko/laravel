<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'Необходимо принять :attribute.',
    'active_url'           => 'Значение :attribute не является допустимым URL-адресом.',
    'after'                => 'Значение :attribute должно являться датой после :date.',
    'after_or_equal'       => 'Значение :attribute должно являться датой после или равной :date.',
    'alpha'                => 'Значение :attribute может сожержать только буквы.',
    'alpha_dash'           => 'Значение :attribute может сожержать только буквы, числа и тире.',
    'alpha_num'            => 'Значение :attribute может сожержать только буквы и числа.',
    'array'                => 'Значение :attribute должно быть массивом.',
    'before'               => 'Значение :attribute должно являться датой до :date.',
    'before_or_equal'      => 'Значение :attribute должно являться датой до или равной :date.',
    'between'              => [
        'numeric' => 'Значение :attribute должно быть от :min до :max.',
        'file'    => 'Размер файла :attribute должен быть от :min до :max кб.',
        'string'  => 'Длина :attribute должна быть от :min до :max символов.',
        'array'   => 'Значение :attribute должно содержать от :min до :max значений.',
    ],
    'boolean'              => ':attribute должно иметь ззначение истина или ложь.',
    'confirmed'            => 'Подтверждение :attribute не совпало.',
    'date'                 => 'Значение :attribute не является действительной датой.',
    'date_format'          => 'Значение :attribute не соответствует формату :format.',
    'different'            => 'Значения :attribute и :other должны отличаться.',
    'digits'               => 'Значение :attribute должно содержать :digits чисел.',
    'digits_between'       => 'Значение :attribute должно содержать от :min до :max чисел.',
    'dimensions'           => 'Файл :attribute имеет недопустимые размеры изображения.',
    'distinct'             => 'Значение поля :attribute имеет дубликат.',
    'email'                => 'Значение :attribute должно быть адресом эл. почты.',
    'exists'               => 'Значение :attribute недействительно.',
    'file'                 => ':attribute должно быть файлом.',
    'filled'               => 'Поле :attribute должно содержать значение.',
    'image'                => ':attribute должно быть изображением.',
    'in'                   => 'Значение :attribute недействительно.',
    'in_array'             => 'Поле :attribute не существует в :other.',
    'integer'              => 'Значение :attribute должно быть целым числом.',
    'ip'                   => 'Значение :attribute должно быть действительным IP адресом.',
    'ipv4'                 => 'Значение :attribute должно быть действительным IPv4 адресом.',
    'ipv6'                 => 'Значение :attribute должно быть действительным IPv6 адресом.',
    'json'                 => 'Значение :attribute должно быть действительной JSON строкой.',
    'max'                  => [
        'numeric' => 'Значение :attribute должно быть больше :max.',
        'file'    => 'Размер :attribute должен быть больше :max кб.',
        'string'  => 'Значение :attribute должно содержать более :max символов.',
        'array'   => 'Значение :attribute должно содержать более :max элементов.',
    ],
    'mimes'                => ':attribute должно быть файлом типа: :values.',
    'mimetypes'            => ':attribute должно быть файлом типа: :values.',
    'min'                  => [
        'numeric' => 'Значение :attribute должно быть больше :min.',
        'file'    => 'Размер :attribute должен быть более :min кб.',
        'string'  => 'Значение :attribute должно содержать от :min символов.',
        'array'   => 'Значение :attribute должно содержать от :min значений.',
    ],
    'not_in'               => 'Значение :attribute недействительно.',
    'numeric'              => 'Значение :attribute должно быть числом.',
    'present'              => 'Значение :attribute должно присутствовать.',
    'regex'                => 'Формат значения поля :attribute неверен.',
    'required'             => 'Поле :attribute обязательно для заполнения.',
    'required_if'          => 'Поле :attribute обязательно для заполнения, когда :other равно :value.',
    'required_unless'      => 'Поле :attribute обязательно для заполнения пока :other не равно :values.',
    'required_with'        => 'Поле :attribute обязательно для заполнения когда присутствует :values.',
    'required_with_all'    => 'Поле :attribute обязательно для заполнения когда присутствует :values.',
    'required_without'     => 'Поле :attribute обязательно для заполнения когда отсутствует :values.',
    'required_without_all' => 'Поле :attribute обязательно для заполнения когда отсутсвуют :values.',
    'same'                 => 'Значения :attribute и :other должны совпадать.',
    'size'                 => [
        'numeric' => 'Длина поля :attribute должна быть :size.',
        'file'    => 'Размер файла :attribute должен быть :size кб.',
        'string'  => 'Длина поля :attribute должна быть :size символов.',
        'array'   => 'Значение поля :attribute должно содержать :size элементов.',
    ],
    'string'               => 'Значение :attribute должно быть строкой.',
    'timezone'             => 'Значение :attribute должно быть в допустимом диапазоне.',
    'unique'               => 'Поле :attribute уже заполнено.',
    'uploaded'             => 'Ошибка загрузки файла :attribute.',
    'url'                  => 'Формат :attribute неверен.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

];
