<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['as' => 'home_tree', 'uses' => 'TreePageController@index' ] );
Route::get('/treedata/', ['as' => 'tree_data', 'uses' => 'TreePageController@data',]);

Route::post('/movenode/', ['as' => 'move_node', 'uses' => 'TreePageController@move',]);
Auth::routes();

Route::get('/list-in-table/', ['as' => 'tablelist', 'uses' => 'TablePageController@index', 'middleware' => 'auth']);
Route::post('/list-in-table/', ['as' => 'workers_list', 'uses' => 'TablePageController@ajaxIndex']);

Route::resource('appointments', 'AppointmentController')->middleware('auth');
Route::post('tabledata', ['as' => 'tabledata', 'uses' => 'AppointmentController@ajaxIndex',]);

Route::post('bosses', [ 'uses' => 'SearchController@getBossList',]);
Route::post('peoples', [ 'uses' => 'SearchController@getPeoplesList',]);
Route::post('positions', [ 'uses' => 'SearchController@getPositionsList',]);