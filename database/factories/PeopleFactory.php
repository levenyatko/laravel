<?php

use Faker\Generator as Faker;

$factory->define( App\Peoples::class, function (Faker $faker) {

    $isMale = 1;
    $gender = 'male';
    if($faker->randomFloat(2,0, 1) > 0.6){
        $isMale = 0;
        $gender = 'female';
    }

    return [
        'first_name' => $faker->firstName($gender),
        'last_name' => $faker->lastName($gender).((!$isMale)?'а':''),
        'isMale' => $isMale,
    ];
});
