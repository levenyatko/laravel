<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('appointments', function (Blueprint $table) {
            $table->foreign('appointment_type_id')->references('type_id')->on('appointment_types')->onUpdate('cascade');
            $table->foreign('position_id')->references('position_id')->on('positions')->onUpdate('cascade');
            $table->foreign('people_id')->references('people_id')->on('peoples')->onUpdate('cascade');
            $table->foreign('appointment_boss_id')->references('appointment_id')->on('appointments')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('appointments', function (Blueprint $table) {
            $table->dropForeign('appointments_appointment_type_id_foreign');
            $table->dropForeign('appointments_people_id_foreign');
            $table->dropForeign('appointments_position_id_foreign');
        });
        Schema::table('positions', function (Blueprint $table) {
            $table->dropForeign('positions_boss_position_id_foreign');
        });
    }
}
