<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CurrentWorkers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
          CREATE VIEW current_workers AS
          (
            select
                    appointments.appointment_id, 
                    v_dates.people_id, peoples.avatar_image_id, peoples.first_name, peoples.last_name, peoples.isMale,  
                    appointments.appointment_type_id, appointments.appointment_boss_id, v_dates.max_date as ap_date, positions.position_id, positions.subdivision_name, positions.position_name, appointments.salary
            from 
                appointments_peoples_max_date as v_dates 
                INNER JOIN appointments on ( appointments.date = v_dates.max_date AND appointments.people_id = v_dates.people_id ) 
                INNER JOIN peoples on peoples.people_id = appointments.people_id 
                INNER JOIN positions ON positions.position_id = appointments.position_id 
            WHERE appointments.appointment_type_id IN (0,1) and appointments.deleted_at is null
          )
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP VIEW IF EXISTS current_workers');
    }
}
