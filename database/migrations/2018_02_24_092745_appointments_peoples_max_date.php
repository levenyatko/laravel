<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

// дата последнего назначения каждого сотрудника
class AppointmentsPeoplesMaxDate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
          CREATE VIEW appointments_peoples_max_date AS
          (
            SELECT people_id, max(date) as max_date
            FROM `appointments`
            where `deleted_at` IS NULL
            GROUP BY people_id
          )
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP VIEW IF EXISTS appointments_peoples_max_date');
    }
}
