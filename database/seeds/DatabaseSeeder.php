<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call( [
                        AppointmentTypesTableSeeder::class ,
                        PositionsTableSeeder::class,
                        PeoplesTableSeeder::class,
                        AppointmentsTableSeeder::class,
                    ]);
    }
}
