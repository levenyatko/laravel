<?php

use Illuminate\Database\Seeder;

class AppointmentTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data_arr = array();
        $test = \App\AppointmentTypes::where('type_id',-1)->count();
        if($test ==0 ){
            $data_arr[] = array('type_id' => -1, 'type_description' => 'увольнение');
        }
        $test = \App\AppointmentTypes::where('type_id',0)->count();
        if($test == 0 ){
            $data_arr[] = array('type_id' => 0, 'type_description' => 'перевод');
        }
        $test = \App\AppointmentTypes::where('type_id',1)->count();
        if($test == 0 ){
            $data_arr[] = array('type_id' => 1, 'type_description' => 'прием');
        }
        if(count($data_arr) > 0) {
            DB::table('appointment_types')->insert($data_arr);
        }
    }
}
