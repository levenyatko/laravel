<?php

use Illuminate\Database\Seeder;
use \Illuminate\Support\Carbon;

class AppointmentsTableSeeder extends Seeder
{
    private $maxSubworkersCount = 10; // максимальное количеcтво подчиненных
    private $minSalary = 2000;
    private $maxSalary = 100000;


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $positions = \App\Positions::all();
        $peoples = \App\Peoples::all();

        $peoples_count = \App\Peoples::count();
        $pos_count =  \App\Positions::count();
        $SubworkersCount = 0;

        $current_position = 0;

        $appointments_ids = array();
        $appointment_boss_id = Null;

        $i = 0;
        while ($i < $peoples_count){

            $SubworkersCount = random_int(1, $this->maxSubworkersCount);
            if($i + $SubworkersCount > $peoples_count){
                $SubworkersCount = $peoples_count - $i;
            }

            for ($j = 0; $j < $SubworkersCount; $j++, $i++ ){
                $data = array();
                $data['position_id'] =  $positions[$current_position]->position_id;
                $data['people_id'] =  $peoples[$i]->people_id;
                $data['appointment_type_id'] = 1;
                $data['date'] = Carbon::create(rand(1960,2015),rand(1,12),rand(1,25));
                $data['salary'] = rand($this->minSalary,$this->maxSalary);
                $data['appointment_boss_id'] = $appointment_boss_id;

                $new_appointment = \App\Appointments::create($data);
                $appointments_ids[] = $new_appointment->id;
            }

            $current_position += 1;
            if($current_position >= $pos_count){
                $current_position = 0;
            }

            if(isset($appointments_ids[0])){
                $appointment_boss_id = array_shift($appointments_ids);
            }
        }

    }
}
