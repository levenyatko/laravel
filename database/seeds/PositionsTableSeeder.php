<?php

use Illuminate\Database\Seeder;

class PositionsTableSeeder extends Seeder
{
    private $maxPositionsCount = 300;
    private $subdivision_names = array('Отдел продаж', 'Отдел 2','Отдел маркетинга', 'Бухгалтерия', 'Администрация');
    private $position_names = array('Менеджер', 'Бухгалтер','Администратор', 'Кассир', 'Торговый представитель');

    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        for ($i = 0 ; $i < $this->maxPositionsCount ; $i++){
            $rand_key1 = array_rand($this->subdivision_names, 1);
            $rand_key2 = array_rand($this->position_names, 1);

            $data = array(
                'subdivision_name' => $this->subdivision_names[$rand_key1],
                'position_name' =>$this->position_names[$rand_key2],
            );
            DB::table('positions')->insert($data);

        }
    }
}
